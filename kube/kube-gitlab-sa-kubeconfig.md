## Generate `.kubeconfig` for gitlab ci/cd with cross cluster access:
1. Create ServiceAccount
```yaml
# part of gitlab-sa.yaml
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
```
2. Create ClusterRoleBinding for ServiceAccount
```yaml
# part of gitlab-sa.yaml
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
```
3. Get ServiceAccount token

```bash
kubectl get secret  $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}') -o jsonpath="{['data']['token']}" -n kube-system | base64 --decode
```
4. Get Cluster CA Certificate in base64
```bash
# DO:
kubectl get secret  $(kubectl -n kube-system get secret | grep etcd-secrets | awk '{print $1}') -o jsonpath="{['data']['etcd-ca']}" -n kube-system
# AWS - ? 
# GCP - ?
```
5. Fill out the `.kubeconfig` template:
```yaml
apiVersion: v1
kind: Config
users:
- name: gitlab-admin
  user:
    token: {{ServiceAccount token}}
clusters:
- cluster:
    certificate-authority-data: {{Cluster CA Certificate in base64}}
    server: https://{{ClusterEndpoint}}
  name: clusterName
contexts:
- context:
    cluster: clusterName
    user: gitlab-admin
  name: clusterName
current-context: clusterName
```
6. Use `.kubeconfig` content as content of Gitlab CI variable with Type: `File` and Name: `KUBECONFIG`

7. Congratulations! Your kubectl is configured on any job in CI/CD! Check it in job script:
```bash
kubectl get pod --all-namespaces
```